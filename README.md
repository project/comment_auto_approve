CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Use case

INTRODUCTION
------------
This module implements auto set comment as approved (published) for users
without "Skip comment approval" permission after certain amount of manually
approved comments.

When user doesn't have "Skip comment approval" permission but already have
certain amount of manually approved comments (by someone who have permission
to approve comments) - comments of this user will be automatically approved
(published) and visible to others.

REQUIREMENTS
------------
* Comment (core module)

RECOMMENDED MODULES
-------------------
None

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------
* Remove "Skip comment approval" permission for all roles which should use
  "Comment Auto Approve" feature
* Add any comment field (or open existing comment field configuration page)
* Scroll down to "Comment Auto Approve" section and open it
* Check "Automatically approve comments after certain amount of manually
  approved comments." checkbox to enable "Comment Auto Approve" feature
* Set "Amount of comments to manual approve" value (should be greater than 0)
* Choose "Approved comments calculation logic" value: Local - count published
  comments of the user which attached to this field only, Global - count all
  published comments added by the user
* Click "Save settings" button

USE CASE
--------
You building "Self regulated" community and would like to approve only first
comments of a new community members.
