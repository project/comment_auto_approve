<?php

namespace Drupal\comment_auto_approve;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Comment Auto Approve handling helper service.
 */
class CommentAutoApprove implements CommentAutoApproveInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CommentAutoApprove object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function preProcessComment(CommentInterface $comment) {

    if ($this->validToApprove($comment) && $this->validCommentsAmount($comment)) {
      $comment->setPublished();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preProcessCommentForm(CommentInterface $comment, array &$form) {

    if ($this->validToApprove($comment)) {
      $field_config = $this->getCommentFieldConfig($comment);

      if ($field_config instanceof FieldConfigInterface && $field_config->getThirdPartySetting('comment_auto_approve', 'enabled')) {
        $button_label = $field_config->getThirdPartySetting('comment_auto_approve', 'add_unapproved_label');

        if ($button_label && !$this->validCommentsAmount($comment) && isset($form['actions'])) {
          $form['actions']['submit']['#value'] = $button_label;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCommentFieldConfig(CommentInterface $comment) {
    $commented_entity = $comment->getCommentedEntity();

    if ($commented_entity instanceof FieldableEntityInterface) {
      $field_definition = $commented_entity->getFieldDefinition($comment->getFieldName());

      if ($field_definition instanceof FieldDefinitionInterface) {

        return $field_definition->getConfig($commented_entity->bundle());
      }
    }

    return NULL;
  }

  /**
   * Is requested comment valid for approvement.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   Comment to check.
   *
   * @return bool
   *   Bool TRUE if comment can be approved or bool FALSE otherwise.
   */
  protected function validToApprove(CommentInterface $comment) {

    if (!$comment->isPublished()) {
      $owner = $comment->getOwner();

      return $owner instanceof UserInterface && !$owner->hasPermission('skip comment approval');
    }

    return FALSE;
  }

  /**
   * Validate published comments amount for comment author.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   Comment to check.
   *
   * @return bool
   *   Bool TRUE if user have enough approved comments or bool FALSE otherwise.
   */
  protected function validCommentsAmount(CommentInterface $comment) {
    $field_config = $this->getCommentFieldConfig($comment);

    if ($field_config instanceof FieldConfigInterface &&
      $field_config->getThirdPartySetting('comment_auto_approve', 'enabled')) {

      $limit = (int) $field_config->getThirdPartySetting('comment_auto_approve', 'manual_approve_qty');
      $logic = $field_config->getThirdPartySetting('comment_auto_approve', 'approved_count_logic');

      $query = $this->entityTypeManager->getStorage($comment->getEntityTypeId())->getQuery();
      $query->accessCheck(TRUE);
      $query->condition('uid', $comment->getOwnerId());
      $query->condition('status', CommentInterface::PUBLISHED);

      if ($logic === CommentAutoApproveInterface::LIMIT_LOCAL) {
        $query->condition('field_name', $comment->getFieldName());
        $query->condition('entity_type', $comment->getCommentedEntityTypeId());
      }

      $query->range(0, $limit);
      $result = $query->execute();

      return count($result) >= $limit;
    }

    return FALSE;
  }

}
