<?php

namespace Drupal\comment_auto_approve;

use Drupal\comment\CommentInterface;

/**
 * Interface of Comment Auto Approve handling helper service.
 */
interface CommentAutoApproveInterface {

  /**
   * Amount of comments to manual approve: Count local (comments per field qty).
   */
  const LIMIT_LOCAL = 'local';

  /**
   * Amount of comments to manual approve: Count global (all comments qty).
   */
  const LIMIT_GLOBAL = 'global';

  /**
   * Approve comment if it should be approved.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   Comment to pre process.
   */
  public function preProcessComment(CommentInterface $comment);

  /**
   * Update comment form.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   Related comment.
   * @param array $form
   *   Form to pre process.
   */
  public function preProcessCommentForm(CommentInterface $comment, array &$form);

  /**
   * Return related comment field configuration.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   Comment to get config field for.
   *
   * @return \Drupal\Core\Field\FieldConfigInterface|null
   *   Return comment field configuration on success or NULL otherwise.
   */
  public function getCommentFieldConfig(CommentInterface $comment);

}
